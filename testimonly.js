function loadjscssfile(filename, filetype) {
  if (filetype == "js") { //if filename is a external JavaScript file
    var fileref = document.createElement('script')
    fileref.setAttribute("type", "text/javascript")
    fileref.setAttribute("src", filename)
  } else if (filetype == "css") { //if filename is an external CSS file
    var fileref = document.createElement("link")
    fileref.setAttribute("rel", "stylesheet")
    fileref.setAttribute("type", "text/css")
    fileref.setAttribute("href", filename)
  }
  if (typeof fileref != "undefined")
    document.getElementsByTagName("head")[0].appendChild(fileref)
}

function removejscssfile(filename, filetype){
    var targetelement=(filetype=="js")? "script" : (filetype=="css")? "link" : "none" //determine element type to create nodelist from
    var targetattr=(filetype=="js")? "src" : (filetype=="css")? "href" : "none" //determine corresponding attribute to test for
    var allsuspects=document.getElementsByTagName(targetelement)
    for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
    if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1)
        allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
    }
}

loadjscssfile("vue.js", "js");
loadjscssfile("botui.js", "js");
loadjscssfile("testimonly.css", "css");

var testimonly = document.createElement('div');
	testimonly.innerHTML = `
	<div id="testimonly-notifier" style="display:none; border-color: rgb(204, 204, 204); margin: -63.5px 0px 0px; padding: 0px; position: fixed; border-width: 1px 0px 1px 1px; top: 50%; z-index: 16776272; background-color: rgb(249, 249, 249); transition: width 0.5s ease 0s; overflow: visible; box-sizing: content-box; width: 26px; right: 0px;">
			<div id="" style="margin: 0px; padding: 7px 3px 33px; cursor: pointer; border-radius: 50px; display: block; background-position: 50% 100%; background-repeat: no-repeat; overflow: visible; border: 1px solid rgb(204, 204, 204); height: 67px; width: 20px; background-color: rgb(0, 0, 200);  z-index: 16776273; box-sizing: content-box; float: left;">
				<div style="margin: 0px !important;letter-spacing: -1px !important; text-spacing: 30px !important; color:white; padding-top: 10px !important; padding-left: 3px !important; width: 20px !important; height: 67px !important; overflow: visible; box-sizing: content-box; writing-mode: vertical-rl; text-orientation: mixed;">
					<h4 id="testimonly-h4">Notification</h4>
					<span id="testimonly-notice" class="testimonly-chat-message-counter">1</span>
				</div>
			</div>
		</div>
		<div id="testimonly-live-chat">
        
				<header id="testimonly-header" class="clearfix text-center">
            
					<a href="#" id="testimonly-cancel" class="testimonly-chat-close">x</a>

					<h4 id="testimonly-company">Company</h4>

				</header>

				<div class="testimonly-chat">
            
					<div class="testimonly-chat-history">
                
						<div class="botui-app-container" id="hello-world">
							<bot-ui></bot-ui>
							<br>
							<br>
							<span id="recommend" class="hide"></span>
							<span id="email" class="hide"></span>
							<span id="apiKey" class="hide"></span>
							<span id="username" class="hide"></span>
							<span id="limit" class="hide"></span>
							<span id="form0" class="hide"></span>
							<span id="form1" class="hide"></span>
							<span id="form2" class="hide"></span>
							<span id="form3" class="hide"></span>
							<span id="form4" class="hide"></span>
							<span id="form5" class="hide"></span>
							<span id="form6" class="hide"></span>
							<span id="form7" class="hide"></span>
							<span id="form8" class="hide"></span>
							<span id="form9" class="hide"></span>
							<span id="form10" class="hide"></span>
							<span id="text0" class="hide"></span>
							<span id="text1" class="hide"></span>
							<span id="text2" class="hide"></span>
							<span id="text3" class="hide"></span>
							<span id="text4" class="hide"></span>
							<span id="text5" class="hide"></span>
							<span id="text6" class="hide"></span>
							<span id="text7" class="hide"></span>
							<span id="text8" class="hide"></span>
							<span id="text9" class="hide"></span>
							<span id="text10" class="hide"></span>
							<span id="state0" class="hide"></span>
							<span id="state1" class="hide"></span>
							<span id="state2" class="hide"></span>
							<span id="state3" class="hide"></span>
							<span id="state4" class="hide"></span>
							<span id="state5" class="hide"></span>
							<span id="state6" class="hide"></span>
							<span id="state7" class="hide"></span>
							<span id="state8" class="hide"></span>
							<span id="state9" class="hide"></span>
							<span id="state10" class="hide"></span>
							<span id="company" class="hide"></span>
						</div>

					</div> <!-- end chat-history -->

					<p class="testimonly-chat-feedback">Powered by Testimonly</p>


				</div> <!-- end chat -->

			
			
		</div>
		
		<!-- Modal -->
		<div id="addtestimonlymodalhere">
		</div>`
			
	
	document.body.appendChild(testimonly);
	
	email = email.replace(/\./g, ',')
	
	let startDate = new Date();
	let elapsedTime = 0;

	const focus = function() {
		startDate = new Date();
	};

	const blur = function() {
		const endDate = new Date();
		const spentTime = endDate.getTime() - startDate.getTime();
		
		
		elapsedTime = parseInt(localStorage.getItem('elapsedTime'));
		if (Number.isInteger(elapsedTime)) {

			elapsedTime += spentTime;
			if (elapsedTime > 600000) {
				localStorage.setItem("elapsedTime",0);
				saveClientTime(elapsedTime)
			}
			else {
				localStorage.setItem("elapsedTime", elapsedTime);
			}
		}
		else {
			localStorage.setItem("elapsedTime", spentTime);
		}
	};

	const beforeunload = function() {
		const endDate = new Date();
		const spentTime = endDate.getTime() - startDate.getTime();

		
		elapsedTime = parseInt(localStorage.getItem('elapsedTime'));
		if (Number.isInteger(elapsedTime)) {
			elapsedTime += spentTime;
			if (elapsedTime > 600000) {
				localStorage.setItem("elapsedTime",0);
				saveClientTime(elapsedTime)
			}
			else {
				localStorage.setItem("elapsedTime", elapsedTime);
			}
		}
		else {
			localStorage.setItem("elapsedTime", spentTime);
		}

		// elapsedTime contains the time spent on page in milliseconds
	};

		var cancel = document.getElementById('testimonly-cancel');
		
		cancel.addEventListener('click', function() {
			document.getElementById('testimonly-live-chat').style.display = 'none';
			document.getElementById('testimonly-notifier').style.display ='none';
		}, false);
	
	
	
	function trackTime(focus,blur,onbeforeunload){
		window.addEventListener('focus',focus);
		window.addEventListener('blur', blur);
		window.addEventListener('beforeunload', beforeunload);
	};
	
	proceed(username,email,apiKey,focus,blur,onbeforeunload);
	
	function proceed(username, email, apiKey,focus,blur,onbeforeunload) {
		getClientData(username, email, apiKey)
		 .then(function (result) {
			var clientData = result.client;
			var userData = result.user;
			var feedbackForm = `<div class="form-group"><label class="control-label">Which of our product category needs urgent improvement?</label><select class="form-control" name="category"><option value="Product Features">Product Features</option><option value="Product Stability">Product Stability</option><option value="Product Design">Product Design</option><option value="Product Pricing">Product Pricing</option><option value="Customer Support">Customer Support</option><option value=""></option></select></div><div class="form-group"><label class="control-label">What exactly should we do to this product category to make you more likely to recommend us?</label><textarea rows="5" name="action" placeholder="" class="form-control" ></textarea></div>`;
			var feedbackText = `Would you mind telling us the one major area of our product where we are currently lagging behind, an area of our product which once fixed will make you more likely to promote us?`;
			document.getElementById('apiKey').innerText = apiKey;
			document.getElementById('email').innerText = email;
			document.getElementById('username').innerText = username;
			document.getElementById('company').innerText = userData.companyName ? userData.companyName : 'Company';
			document.getElementById('form0').innerText = userData.zero ? userData.zero : feedbackForm ;
			document.getElementById('form1').innerText = userData.one ? userData.one : feedbackForm ;
			document.getElementById('form2').innerText = userData.two ? userData.two : feedbackForm ;
			document.getElementById('form3').innerText = userData.three ? userData.three : feedbackForm ;
			document.getElementById('form4').innerText = userData.four ? userData.four : feedbackForm ;
			document.getElementById('form5').innerText = userData.five ? userData.five : feedbackForm ;
			document.getElementById('form6').innerText = userData.six ? userData.six : feedbackForm ;
			document.getElementById('form7').innerText = userData.seven ? userData.seven : feedbackForm ;
			document.getElementById('form8').innerText = userData.eighth ? userData.eighth : feedbackForm ;
			document.getElementById('form9').innerText = userData.nine ? userData.nine : '' ;
			document.getElementById('form10').innerText = userData.ten ? userData.ten : '' ;
			document.getElementById('text0').innerText = userData.zeroText ? userData.zeroText : feedbackText ;
			document.getElementById('text1').innerText = userData.oneText ? userData.oneText : feedbackText ;
			document.getElementById('text2').innerText = userData.twoText ? userData.twoText : feedbackText ;
			document.getElementById('text3').innerText = userData.threeText ? userData.threeText : feedbackText ;
			document.getElementById('text4').innerText = userData.fourText ? userData.fourText : feedbackText ;
			document.getElementById('text5').innerText = userData.fiveText ? userData.fiveText : feedbackText ;
			document.getElementById('text6').innerText = userData.sixText ? userData.sixText : feedbackText ;
			document.getElementById('text7').innerText = userData.sevenText ? userData.sevenText : feedbackText ;
			document.getElementById('text8').innerText = userData.eighthText ? userData.eighthText : feedbackText ;
			document.getElementById('text9').innerText = userData.nineText ? userData.nineText : '' ;
			document.getElementById('text10').innerText = userData.tenText ? userData.tenText : '' ;
			document.getElementById('state0').innerText = (userData.zeroState == true) ?  userData.zeroState : false ;
			document.getElementById('state1').innerText = (userData.oneState == true) ?  userData.oneState : false ;
			document.getElementById('state2').innerText = (userData.twoState == true) ?  userData.twoState : false ;
			document.getElementById('state3').innerText = (userData.threeState == true) ?  userData.threeState : false ;
			document.getElementById('state4').innerText = (userData.fourState == true) ?  userData.fourState : false ;
			document.getElementById('state5').innerText = (userData.fiveState == true) ?  userData.fiveState : false ;
			document.getElementById('state6').innerText = (userData.sixState == true) ?  userData.sixState : false ;
			document.getElementById('state7').innerText = (userData.sevenState == true) ?  userData.sevenState : false ;
			document.getElementById('state8').innerText = (userData.eighthState == true) ?  userData.eighthState : false ;
			document.getElementById('state9').innerText = (userData.nineState == true) ?  userData.nineState : false ;
			document.getElementById('state10').innerText = (userData.tenState == true) ?  userData.tenState : false ;
			document.getElementById('limit').innerText = userData.limit ;
			if (clientData === null) {
			}
			else {
				if ((!clientData.requestStatus) && (clientData.timeSpentInSec >= (userData.limit * 60))) {
					displayNotifier(document.getElementById('company').innerText);
					getFeedback();
					console.log(9);
				}
				else if((!clientData.requestStatus) && (clientData.timeSpentInSec < (userData.limit * 60))) {
					trackTime(focus,blur,onbeforeunload);
				}
				else {
				}
			}
		})
		
	};
	
	async function getClientData(username, email, apiKey) {
		
		var url = new URL('https://us-central1-testimonly-7f52e.cloudfunctions.net/getClientData')
		
		var date = new Date();
		//start
		var day = date.getDate();
		var month = date.getMonth()+1; //January is 0!
		var year = date.getFullYear();
		var hour = (date.getHours()<10?'0':'') + date.getHours();
		var minute = (date.getMinutes()<10?'0':'') + date.getMinutes();
		//end
		
		var parsedDate = day + '/' + month + '/' + year
		console.log(parsedDate);
		var params = {username:username, email:email, apiKey:apiKey, parsedDate:parsedDate} // or:

		url.search = new URLSearchParams(params)

		let response = await fetch(url)
				
		let data = await response.json();			
	
		return data;
	};
	
	function getFeedback() { 
		var botui = new BotUI('hello-world');
		var company = document.getElementById('company').innerText
		if (company =='Company'){
			company = 'us';
		}
		botui.message.add({ // show a message
			cssClass: 'bot-class',
			content: 'How likely are you to recommend '+ company +' to a friend or colleague?'
		}).then(function () { // wait till its shown
			return botui.action.button({
				addMessage: false,
				delay: 1000,
				action: [{
					text: '0',
					value: '0'
				},{
					text: '1',
					value: '1'
				}, {
					text: '2',
					value: '2'
				}, {
					text: '3',
					value: '3'
				}, {
					text: '4',
					value: '4'
				}, {
					text: '5',
					value: '5'
				}, {
					text: '6',
					value: '6'
				}, {
					text: '7',
					value: '7'
				}, {
					text: '8',
					value: '8'
				}, {
					text: '9',
					value: '9'
				}, {
					text: '10',
					value: '10'
				}]
			})
		}).then(function (res) {
			document.getElementById("recommend").innerText = res.value;
			var check = 'text' + res.value;
			var check2 = 'state' + res.value;
			var text_ = document.getElementById(check).innerText;
			var proceed = (document.getElementById(check2).innerText == 'true');
			botui.message.add({ // show a message
				human: true,
				cssClass: 'human-class',
				content: res.value
			});
			if(proceed) {
			  botui.message.add({
				delay: 1000,
				content: text_,
				cssClass: 'bot-class',
			  }).then(function () {
				return botui.action.button({
					addMessage: false,
					delay: 1000,
					action: [{
						text: 'Okay, No Problem',
						value: 'Yes'
					}, {
						text: 'Not Interested',
						value: 'No'
					}]
				}).then(function (res) {
					if (res.value == 'Yes'){
						document.getElementById('testimonly-live-chat').style.display = 'none';
						document.getElementById('testimonly-notifier').style.display ='none';
						var recommend = document.getElementById('recommend').innerText;
						var formHtml = ('form' + recommend)
						var formHtml = document.getElementById(formHtml).innerText;
						getFormData(formHtml)
					}
					else {
						document.getElementById('testimonly-live-chat').style.display = 'none';
						document.getElementById('testimonly-notifier').style.display ='none';
						saveRequestData()
					}
				})
			  })
			}
			else {
			  document.getElementById('testimonly-live-chat').style.display = 'none';
			  document.getElementById('testimonly-notifier').style.display ='none';
			  saveRequestData()
			}
				
		})
	}
	
	
	async function saveRequestData() {
		var date = new Date();
		//start
		var day = date.getDate();
		var month = date.getMonth()+1; //January is 0!
		var year = date.getFullYear();
		var hour = (date.getHours()<10?'0':'') + date.getHours();
		var minute = (date.getMinutes()<10?'0':'') + date.getMinutes();
		//end
		var parsedDate = day + '/' + month + '/' + year;
		var time = hour + ':' + minute
		var recommend = document.getElementById('recommend').innerText;
		var email = document.getElementById('email').innerText;
		var apiKey = document.getElementById('apiKey').innerText;
		var url = new URL('https://us-central1-testimonly-7f52e.cloudfunctions.net/saveRequestData');
		var params = { email:email, apiKey:apiKey, time:time, parsedDate:parsedDate, recommend:recommend}; // or:
		
		url.search = new URLSearchParams(params);

		let response = await fetch(url);
				
		let data = await response.json();			
	

	};
	
	
	function displayNotifier(company) {
		if (company) {
			document.getElementById('testimonly-company').innerText = company;
		}
		var notifier = document.getElementById('testimonly-notifier');
		notifier.style.display = 'block';
		var number = document.getElementById('testimonly-notice');
		number.style.display = 'block';
		notifier.addEventListener('click', function() {
			document.getElementById('testimonly-live-chat').style.display = 'block';
			document.getElementById('testimonly-notice').style.display ='none';
		}, false);
	};
	
	function getFormData(formHtml) {
		loadjscssfile("https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css", "css");
		loadjscssfile("https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js", "js");
		loadjscssfile("https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js", "js");

		document.getElementById('addtestimonlymodalhere').innerHTML=`
<div id="testimonlyModal" class="testimonly-modal">
  <!-- Modal content -->
  <div class="testimonly-modal-content">
	<span class="testimonly-close" onclick="closeTestimonlyModal()">&times;</span>
	<div class="row">
		<div class="col-3"></div>
		<div class="col-6">
			<h2 id"testimonly-form-heading" class="text-center"></h2>
			<form id="testimonly-form" >`
				+ formHtml + `
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-6">
						<button class="btn btn-lg btn-block btn-primary" onclick="saveForm(event)">Submit</button>
					</div>
					<div class="col-md-3"></div>
				</div>
		</div>
		<div class="col-3"></div>
	</div>
  </div>
</div>`
		var modal = document.getElementById('testimonlyModal');
		modal.style.display = "block";
	}
	
	$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    var $radio = $('input[type=radio],input[type=checkbox]',this);
    $.each($radio,function(){
        if(!o.hasOwnProperty(this.name)){
            o[this.name] = '';
        }
    });
    return o;
};


function saveForm(e){
  e.preventDefault();
  var obj = $("#testimonly-form").serializeObject();
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) {
        if(Array.isArray(obj[key])){
			var value = String(obj[key]);
			obj[key] = "[" + value + "]";
		}
    }
  }
  console.log(obj);
  saveFormData(obj)
  closeTestimonlyModal();
}

async function saveClientTime(time) {
		var email = document.getElementById('email').innerText;
		var apiKey = document.getElementById('apiKey').innerText;
		var minutes = Math.floor(time / 60000)
		var seconds = Math.floor(time / 1000)
		var url = new URL('https://us-central1-testimonly-7f52e.cloudfunctions.net/updateClientTime');
		var params = {email:email, apiKey:apiKey, minutes:minutes, seconds:seconds};
		
		url.search = new URLSearchParams(params);
		
		let response = await fetch(url);
		
		let data = await response.json();
		
	}

async function saveFormData(obj) {
		var date = new Date();
		//start
		var day = date.getDate();
		var month = date.getMonth()+1; //January is 0!
		var year = date.getFullYear();
		var hour = (date.getHours()<10?'0':'') + date.getHours();
		var minute = (date.getMinutes()<10?'0':'') + date.getMinutes();
		//end
		var parsedDate = day + '/' + month + '/' + year;
		var time = hour + ':' + minute;
		obj.parsedDate = parsedDate;
		obj.time = time;
		var recommend = document.getElementById('recommend').innerText;
		var email = document.getElementById('email').innerText;
		var newEmail = email.replace(/,/g, '.');
		var apiKey = document.getElementById('apiKey').innerText;
		obj.email = newEmail;
		var feedback = JSON.stringify(obj)
		var url = new URL('https://us-central1-testimonly-7f52e.cloudfunctions.net/saveFormData');
		var params = { email:email, parsedDate : parsedDate, time : time, apiKey:apiKey, recommend:recommend, feedback:feedback, }; // or:
		
		url.search = new URLSearchParams(params);

		let response = await fetch(url);
				
		let data = await response.json();			
	
	};

function closeTestimonlyModal(){
  removejscssfile("https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css", "css");
  removejscssfile("https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js", "js");
  removejscssfile("https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js", "js");
  var modal = document.getElementById('testimonlyModal');
  modal.style.display = "none";
}