from PIL import Image
android_chrome = [192,512]
favicon = [16,32]
favicon_nil = [32,]
apple_touch_icon_nil = [180,]
apple_touch_icon_no = [57,60,72,76,114,120,144,152,180]
apple_touch_icon_precomposed_no = [57,60,72,76,114,120,144,152,180]
apple_touch_icon_precomposed_nil = [180,]
logo = [32,]


for i in android_chrome:
	img = Image.open('testimonly.png')
	new_img = img.resize((i,i))
	new_img.save('android-chrome-{a}x{a}.png'.format(a = i))
	
for i in favicon:
	img = Image.open('testimonly.png')
	new_img = img.resize((i,i))
	new_img.save('favicon-{a}x{a}.png'.format(a = i))
	
for i in favicon_nil:
	img = Image.open('testimonly.png')
	new_img = img.resize((i,i))
	new_img.save('favicon.png')
	
for i in apple_touch_icon_nil:
	img = Image.open('testimonly.png')
	new_img = img.resize((i,i))
	new_img.save('apple-touch-icon.png')
	
for i in apple_touch_icon_no:
	img = Image.open('testimonly.png')
	new_img = img.resize((i,i))
	new_img.save('apple-touch-icon-{a}x{a}.png'.format(a = i))
	
for i in apple_touch_icon_precomposed_no:
	img = Image.open('testimonly.png')
	new_img = img.resize((i,i))
	new_img.save('apple-touch-icon-{a}x{a}-precomposed.png'.format(a = i))
	
for i in apple_touch_icon_precomposed_nil:
	img = Image.open('testimonly.png')
	new_img = img.resize((i,i))
	new_img.save('apple-touch-icon-precomposed.png')
	
for i in logo:
	img = Image.open('testimonly.png')
	new_img = img.resize((i,i))
	new_img.save('logo.png')