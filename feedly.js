	var testimonly = document.createElement('div');
	testimonly.innerHTML = `
	<div id="testimonly-notifier" style="display:none; border-color: rgb(204, 204, 204); margin: -63.5px 0px 0px; padding: 0px; position: fixed; border-width: 1px 0px 1px 1px; top: 50%; z-index: 16776272; background-color: rgb(249, 249, 249); transition: width 0.5s ease 0s; overflow: visible; box-sizing: content-box; width: 26px; right: 0px;">
			<div id="" style="margin: 0px; padding: 7px 3px 33px; cursor: pointer; border-radius: 50px; display: block; background-position: 50% 100%; background-repeat: no-repeat; overflow: visible; border: 1px solid rgb(204, 204, 204); height: 67px; width: 20px; background-color: rgb(0, 0, 200);  z-index: 16776273; box-sizing: content-box; float: left;">
				<div style="margin: 0px;letter-spacing: -1px; text-spacing: 30px; color:white; padding-top: 10px; padding-left: 3px; width: 20px; height: 67px; overflow: visible; box-sizing: content-box; writing-mode: vertical-rl; text-orientation: mixed;">
					<h4>Notification</h4>
					<span id="testimonly-notice" class="testimonly-chat-message-counter">1</span>
				</div>
			</div>
		</div>
		<div id="testimonly-live-chat">
        
				<header id="testimonly-header" class="clearfix text-center">
            
					<a href="#" id="testimonly-cancel" class="testimonly-chat-close">x</a>

					<h4 id="testimonly-company">Company</h4>

				</header>

				<div class="testimonly-chat">
            
					<div class="testimonly-chat-history">
                
						<div class="botui-app-container" id="hello-world">
							<bot-ui></bot-ui>
							<br>
							<br>
							<span id="experience" class="hide"></span>
							<span id="testimonial" class="hide"></span>
							<span id="tag" class="hide"></span>
							<span id="improve" class="hide"></span>
							<span id="email" class="hide"></span>
							<span id="apiKey" class="hide"></span>
							<span id="username" class="hide"></span>
							<span id="limit" class="hide"></span>
							<span id="total" class="hide"></span>
							<span id="testimonialNo" class="hide"></span>
							<span id="twitter" class="hide"></span>
							<span id="brandname" class="hide"></span>
							<span id="brandposition" class="hide"></span>
							<span id="brandwebsite" class="hide"></span>
							<span id="saveBrand" class="hide"></span>
						</div>

					</div> <!-- end chat-history -->

					<p class="testimonly-chat-feedback">Powered by Testimonly</p>


				</div> <!-- end chat -->

			
			
		</div>`
			
	
	document.body.appendChild(testimonly);
	
	email = email.replace(/\./g, ',')
	
	let startDate = new Date();
	let elapsedTime = 0;

	const focus = function() {
		startDate = new Date();
	};

	const blur = function() {
		const endDate = new Date();
		const spentTime = endDate.getTime() - startDate.getTime();
		
		
		elapsedTime = parseInt(localStorage.getItem('elapsedTime'));
		if (Number.isInteger(elapsedTime)) {

			elapsedTime += spentTime;
			if (elapsedTime > 600000) {
				localStorage.setItem("elapsedTime",0);
				saveClientTime(elapsedTime)
			}
			else {
				localStorage.setItem("elapsedTime", elapsedTime);
			}
		}
		else {
			localStorage.setItem("elapsedTime", spentTime);
		}
	};

	const beforeunload = function() {
		const endDate = new Date();
		const spentTime = endDate.getTime() - startDate.getTime();
		elapsedTime += spentTime;

		
		elapsedTime = parseInt(localStorage.getItem('elapsedTime'));
		if (Number.isInteger(elapsedTime)) {
			elapsedTime += spentTime;
			if (elapsedTime > 600000) {
				localStorage.setItem("elapsedTime",0);
				saveClientTime(elapsedTime)
			}
			else {
				localStorage.setItem("elapsedTime", elapsedTime);
			}
		}
		else {
			localStorage.setItem("elapsedTime", spentTime);
		}

		// elapsedTime contains the time spent on page in milliseconds
	};

		var cancel = document.getElementById('testimonly-cancel');
		
		cancel.addEventListener('click', function() {
			document.getElementById('testimonly-live-chat').style.display = 'none';
			document.getElementById('testimonly-notifier').style.display ='none';
			submitFeedback();	
		}, false);
	
	
	
	function trackTime(focus,blur,onbeforeunload){
		window.addEventListener('focus',focus);
		window.addEventListener('blur', blur);
		window.addEventListener('beforeunload', beforeunload);
	};
	
	setTimeout(proceed,2000,username,email,apiKey,determine,collectTestimonial,focus,blur,onbeforeunload);
	
	
	
	function proceed(username, email, apiKey,determine,collectTestimonial,focus,blur,onbeforeunload) {
		var date = new Date();
		getClientData(username, email, apiKey)
		 .then(function (result) {
			var clientData = result.client;
			var userData = result.user;
			document.getElementById('total').innerText = clientData ? clientData.totalFeedbacks : '',
			document.getElementById('apiKey').innerText = apiKey;
			document.getElementById('email').innerText = email;
			document.getElementById('username').innerText = username;
			document.getElementById('limit').innerText = userData.limit * 60000;
			document.getElementById('testimonialNo').innerText = userData.totalTestimonial;
			if (clientData === null) {
			}
			else if (determine) {
				if (!clientData.testimonialStatus && collectTestimonial) {
					displayNotifier(userData.companyName);
					getTestimonial();
				}
			}
			else {
				if ((!clientData.testimonialStatus) && (clientData.timeSpentInSec >= (userData.limit * 60000))) {
					if (clientData.totalFeedbacks == 0) {
						displayNotifier(userData.companyName);
						getTestimonial();
					}
					else {
						var newLimit = (clientData.totalFeedbacks + 1) * (userData.limit * 60000);
						if (clientData.timeSpentInSec >= newLimit) {
							displayNotifier(userData.companyName);
							getTestimonial();
							
						}
						else {
							trackTime(focus,blur,onbeforeunload);
							console.log(0);
							displayNotifier(userData.companyName);
							getTestimonial();
						}
					}
				}
				else if((clientData.testimonialStatus) && (clientData.timeSpentInSec >= (userData.limit * 60000))) {
					var newLimit = ((clientData.totalFeedbacks + 2) * (userData.limit * 60000));
					console.log(newLimit);
					if (clientData.timeSpentInSec >= newLimit) {
						console.log(3);
						displayNotifier(userData.companyName);
						getSubsequentFeedback();
					}
					else {
						trackTime(focus,blur,onbeforeunload);
						console.log(2);
					}
				}
					
				else {
					trackTime(focus,blur,onbeforeunload);
					console.log(1);
				}
			}
		})
		
	};
	
	async function getClientData(username, email, apiKey, date) {
		
		var url = new URL('https://us-central1-testimonly-7f52e.cloudfunctions.net/getClientData')
		
		var date = new Date();

		var params = {username:username, email:email, apiKey:apiKey, date:date} // or:

		url.search = new URLSearchParams(params)

		let response = await fetch(url)
				
		let data = await response.json();			
	
		return data;
	};
	
	
	function getSubsequentFeedback() { 
		var botui = new BotUI('hello-world');
		botui.message.add({ // show a message
			cssClass: 'bot-class',
			content: 'Are you still very likely to recommend us?'
		}).then(function () { // wait till its shown
			return botui.action.button({
				addMessage: false,
				delay: 1000,
				action: [{
					text: 'Yes!',
					value: 'Yes!'
				}, {
					text: 'No!',
					value: 'No!'
				}]
			})
		}).then(function (res) {
			botui.message.add({ // show a message
				human: true,
				cssClass: 'human-class',
				content: res.value
			})
			document.getElementById("experience").innerText = res.value;
			if (res.value == 'Yes!') {
				botui.message.add({
					content: 'What part of our service do you think we should improve on?'
				}).then(function () {
					return botui.action.button({
						addMessage: false,
						delay: 1000,
						action: [{
							text: 'Our Support',
							value: 'Support!'
						}, {
							text: 'Our Design!',
							value: 'Design!'
						},{
							text: 'Our Customer Support',
							value: 'Customer Support'
						},{
							text: 'Other!',
							value: 'Other!'
						}]
					})
				}).then(function (res) { // get the result
					if(res.value == 'Other!') {
						botui.action.text({
							addMessage: false,
							delay: 1000,
							action: {
								sub_type: 'text',
								placeholder: 'Enter your feedback here'
							}
						}).then(function (res) {
							document.getElementById("improve").innerText = res.value;
							botui.message.add({
								human: true,
								content: res.value
							});
						}).then(function (res) {
							botui.message.add({
								delay: 1000,
								content: '!(check) All Done, Thank You For Your Feedback!'
							});
						}).then(function () {
							submitFeedback();
						})
					}
			
					else {
						document.getElementById("improve").innerText = res.value;
						botui.message.add({
							human: true,
							content: 'Your ' + res.value
						}).then(function () {
							botui.message.add({
								delay: 1000,
								content: '!(check) All Done, Thank You For Your Feedback!'
							});
						}).then(function () {
							submitFeedback();
						})
					}	
				})
			}
			
			else {
			  botui.message.add({
				content: 'On a scale of 1-10, how currently likely are you to recommend us?'
			  }).then(function () {
				return botui.action.button({
					addMessage: false,
					delay: 1000,
					action: [{
						text: '1',
						value: '1'
					}, {
						text: '2',
						value: '2'
					}, {
						text: '3',
						value: '3'
					}, {
						text: '4',
						value: '4'
					}, {
						text: '5',
						value: '5'
					}, {
						text: '6',
						value: '6'
					}, {
						text: '7',
						value: '7'
					}, {
						text: '8',
						value: '8'
					}, {
						text: '9',
						value: '9'
					}, {
						text: '10',
						value: '10'
					}]
				})
	     	  }).then(function (res) {
				botui.message.add({ // show a message
					human: true,
					cssClass: 'human-class',
					content: res.value
				})
			  }).then(function () {
				botui.message.add({
					addMessage: false,
					delay: 1000,
					content: 'What part of our service do you think we should improve on?'
				}).then(function () {
					return botui.action.button({
						addMessage: false,
						delay: 1000,
						action: [{
							text: 'Our Support',
							value: 'Support!'
						}, {
							text: 'Our Design!',
							value: 'Design!'
						},{
							text: 'Our Customer Support',
							value: 'Customer Support'
						},{
							text: 'Other!',
							value: 'Other!'
						}]
					})
				}).then(function (res) { // get the result
					if(res.value == 'Other!') {
						botui.action.text({
							addMessage: false,
							delay: 1000,
							action: {
								sub_type: 'text',
								placeholder: 'Enter your feedback here'
							}
						}).then(function (res) {
							document.getElementById("improve").innerText = res.value;
							botui.message.add({
								human: true,
								content: res.value
							});
						}).then(function (res) {
							botui.message.add({
								delay: 1000,
								content: '!(check) All Done, Thank You For Your Feedback!'
							});
						}).then(function () {
							submitFeedback();
						})
					}
			
					else {
						document.getElementById("improve").innerText = res.value;
						botui.message.add({
							human: true,
							content: 'Your ' + res.value
						}).then(function () {
							botui.message.add({
								delay: 1000,
								content: '!(check) All Done, Thank You For Your Feedback!'
							});
						}).then(function () {
							submitFeedback();
						})
					}	
				})
			  })
			}
		
		})		
	};
	
	function getTestimonial() { 
		var botui = new BotUI('hello-world');

		botui.message.add({ // show a message
			cssClass: 'bot-class',
			content: 'On a scale of 1-10, how likely are you to recommend us?'
		}).then(function () { // wait till its shown
			return botui.action.button({
				addMessage: false,
				delay: 1000,
				action: [{
					text: '1',
					value: '1'
				}, {
					text: '2',
					value: '2'
				}, {
					text: '3',
					value: '3'
				}, {
					text: '4',
					value: '4'
				}, {
					text: '5',
					value: '5'
				}, {
					text: '6',
					value: '6'
				}, {
					text: '7',
					value: '7'
				}, {
					text: '8',
					value: '8'
				}, {
					text: '9',
					value: '9'
				}, {
					text: '10',
					value: '10'
				}]
			})
		}).then(function (res) {
			botui.message.add({ // show a message
				human: true,
				cssClass: 'human-class',
				content: res.value
			})
			document.getElementById("experience").innerText = res.value;
			if (res.value == "7" || res.value == "8" || res.value == "9" || res.value == "10"){
				botui.message.add({
					cssClass: 'bot-class',
					delay: 1000,
					content: 'We are very delighted to hear that you are very likely to recommend us'
				}).then(function () {
					botui.message.add({
						cssClass: 'bot-class',
						delay: 1000,
						content: 'And we would absolutely love to have your testimonial'
					});
				}).then(function () {
					botui.message.add({
						cssClass: 'bot-class',
						delay: 2000,
						content: 'On our landing page in four quick questions'
					})
				}).then(function () { // wait till its shown
					return botui.action.button({
						addMessage: false,
						delay: 1000,
						action: [{
							text: 'Proceed To Testimonial',
							value: 'Proceed'
						}, {
							text: 'Do Not Proceed To Testimonial',
							value: 'Do Not Proceed'
						}]
					})
				}).then(function (res) {
						botui.message.add({ // show a message
							human: true,
							cssClass: 'human-class',
							content: res.value
						})
						if (res.value == 'Proceed') {
							getClientTestimonial(botui);
						}
						else {
							getFeedback(botui);
						}
				})
				
				
			}
			
			else {
				botui.message.add({
					content: 'What part of our service do you think we should improve on?'
				}).then(function () {
					return botui.action.button({
						addMessage: false,
						delay: 1000,
						action: [{
							text: 'Our Support',
							value: 'Support!'
						}, {
							text: 'Our Design!',
							value: 'Design!'
						},{
							text: 'Our Customer Support',
							value: 'Customer Support'
						},{
							text: 'Other!',
							value: 'Other!'
						}]
					})
				}).then(function (res) { // get the result
					if(res.value == 'Other!') {
						botui.action.text({
							addMessage: false,
							delay: 1000,
							action: {
								sub_type: 'text',
								placeholder: 'Enter your feedback here'
							}
						}).then(function (res) {
							document.getElementById("improve").innerText = res.value;
							botui.message.add({
								human: true,
								content: res.value
							});
						}).then(function (res) {
							botui.message.add({
								delay: 1000,
								content: '!(check) All Done, Thank You For Your Feedback!'
							});
						}).then(function () {
							submitFeedback();
						})
					}
			
					else {
						document.getElementById("improve").innerText = res.value;
						botui.message.add({
							human: true,
							content: 'Your ' + res.value
						}).then(function () {
							botui.message.add({
								delay: 1000,
								content: '!(check) All Done, Thank You For Your Feedback!'
							});
						}).then(function () {
							submitFeedback();
						})
					}	
				})
			}
		})		
	};
	
	
	var getFeedback = function(botui) {
				botui.message.add({
					content: 'What part of our service do you think we should improve on?'
				}).then(function () {
					return botui.action.button({
						addMessage: false,
						delay: 1000,
						action: [{
							text: 'Our Support',
							value: 'Support!'
						}, {
							text: 'Our Design!',
							value: 'Design!'
						},{
							text: 'Our Customer Support',
							value: 'Customer Support'
						},{
							text: 'Other!',
							value: 'Other!'
						}]
					})
				}).then(function (res) { // get the result
					if(res.value == 'Other!') {
						botui.action.text({
							addMessage: false,
							delay: 1000,
							action: {
								sub_type: 'text',
								placeholder: 'Enter your feedback here'
							}
						}).then(function (res) {
							document.getElementById("improve").innerText = res.value;
							botui.message.add({
								human: true,
								content: res.value
							});
						}).then(function (res) {
							botui.message.add({
								delay: 1000,
								content: '!(check) All Done, Thank You For Your Feedback!'
							});
						}).then(function () {
							submitFeedback();
						})
					}
			
					else {
						document.getElementById("improve").innerText = res.value;
						botui.message.add({
							human: true,
							content: 'Your ' + res.value
						}).then(function () {
							botui.message.add({
								delay: 1000,
								content: '!(check) All Done, Thank You For Your Feedback!'
							});
						}).then(function () {
							submitFeedback();
						})
					}	
				})
	}
	
	var getClientTestimonial = function(botui) {
			botui.message.add({
				cssClass: 'bot-class',
				delay: 3000,
				content: 'Question 1 '
			}).then(function () {
				  botui.message.add({
					cssClass: 'bot-class',
					delay: 1000,
					content: 'What exactly do you like the most about our service'
				  })
				}).then(function () {
					return botui.action.text({
						delay: 4000,
						addMessage: false,
						action: {
							sub_type: 'text',
							placeholder: 'Enter your text here'
						}
					})
				}).then(function (res) {
					document.getElementById("testimonial").innerText = res.value;
					botui.message.add({
						human: true,
						content: res.value
					});
				}).then(function () {
					botui.message.add({
						cssClass: 'question-class',
						delay: 1000,
						content: 'Question 2 '
					}).then(function () {
						botui.message.add({
							cssClass: 'bot-class',
							content: 'Select a tag that summarizes your text above'
						})
					})
				}).then(function () {
						return botui.action.button({
								addMessage: false,
								delay: 2000,
								action: [{
									text: 'General Service',
									value: 'General Service!'
								}, {
									text: 'Design!',
									value: 'Design!'
								},{
									text: 'Customer Support',
									value: 'Customer Support'
								},{
									text: 'Other!',
									value: 'Other!'
								}]
							})
				}).then(function (res) { // get the result
							if(res.value == 'Other!') {
								botui.action.text({
									addMessage: false,
									delay: 1000,
									action: {
										sub_type: 'text',
										placeholder: 'Enter tag here'
									}
								}).then(function (res) {
									document.getElementById("tag").innerText = res.value;
									botui.message.add({
										human: true,
										content: res.value
									});
								}).then(function () {
									botui.message.add({
										cssClass: 'bot-class',
										delay: 2000,
										content: 'Question 3'
									})
								}).then(function () {
										botui.message.add({
											cssClass: 'bot-class',
											delay: 3000,
											content: 'We would like to have your twitter account link'
										})
								}).then(function() {
										botui.message.add({
											cssClass: 'bot-class',
											delay: 4000,
											content: 'As we will like to showcase this as a testimonial on our front page'
										})
								}).then(function() {
										botui.message.add({
											cssClass: 'bot-class',
											delay: 5000,
											content: 'And we will need your twitter username as proof'
										})
											
								}).then(function() {
										return botui.action.text({
											delay: 1000,
											addMessage: false,
											action: {
												sub_type: 'text',
												placeholder: 'https://www.twitter.com/'
											}
										})
								}).then(function(res) {
										document.getElementById("twitter").innerText = res.value;
										botui.message.add({
											delay: 1000,
											human: true,
											content: res.value
										});
								}).then(function() {
										botui.message.add({
											delay: 2000,
											content: 'Question 4'
										});
								}).then(function() {
										botui.message.add({
											delay: 3000,
											content: 'Are you in any way utilizing our service'
										});	
										botui.message.add({
											delay: 4000,
											content: 'in affiliation with a Company Brand or as an Individual'
										});	
								}).then(function() {
										return botui.action.button({
											addMessage: false,
											delay: 5000,
											action: [{
												text: 'Company Brand',
												value: 'Brand'
											}, {
												text: 'Individual',
												value: 'Individual'
											}]
										})
								}).then(function(res) {
										if (res.value == 'Brand') {
											botui.message.add({
												human: true,
												content: res.value
											});
											botui.message.add({
												content: 'Name Of Brand'
											}).then(function() {
												return botui.action.text({
													delay: 1000,
													addMessage: false,
													action: {
														sub_type: 'text',
														placeholder: 'Brand Name'
													}
												})
											}).then(function(res) {
												document.getElementById('brandname').innerText = res.value;
												botui.message.add({
													delay: 2000,
													human: true,
													content: res.value
												});
												botui.message.add({
													delay: 3000,
													content: 'Website Of This Brand'
												});
											}).then(function() {
												return botui.action.text({
													delay: 4000,
													addMessage: false,
													action: {
														sub_type: 'text',
														placeholder: 'https://'
													}
												})
											}).then(function(res) {
												document.getElementById('brandwebsite').innerText = res.value;
												botui.message.add({
													human: true,
													content: res.value
												});
												botui.message.add({
													delay: 1000,
													content: 'Your Position At This Brand'
												});
											}).then(function() {
												return botui.action.text({
													delay: 2000,
													addMessage: false,
													action: {
														sub_type: 'text',
														placeholder: 'Brand Position'
													}
												})
											}).then(function(res) {
												document.getElementById('brandposition').innerText = res.value;
												botui.message.add({
													human: true,
													content: res.value
												});
											}).then(function () {
												botui.message.add({
													delay: 1000,
													content: '!(check) All Done, Thank You For Your Testimonial!'
												});
											}).then(function () {
												submitTestimonial();
											})
										}
										
										else {
											botui.message.add({
												delay: 1000,
												content: '!(check) All Done, Thank You For Your Testimonial!'
											}).then(function () {
												submitTestimonial();
											})
										}
								})
							}
							else {
								document.getElementById("tag").innerText = res.value;
								botui.message.add({
									human: true,
									content: res.value
								}).then(function () {
									botui.message.add({
										cssClass: 'bot-class',
										delay: 1000,
										content: 'Question 3'
									})
								}).then(function (res) {
										botui.message.add({
											cssClass: 'bot-class',
											delay: 2000,
											content: 'We would like to have your twitter account link'
										})
								}).then(function() {
											botui.message.add({
												cssClass: 'bot-class',
												delay: 3000,
												content: 'As we will like to showcase this as a testimonial on our front page'
											})
								}).then(function() {
											botui.message.add({
												cssClass: 'bot-class',
												delay: 4000,
												content: 'And we will need your twitter link as proof'
											})
											
								}).then(function() {
										return botui.action.text({
											delay: 5000,
											addMessage: false,
											action: {
												sub_type: 'text',
												placeholder: 'Enter your text here'
											}
										})
								}).then(function(res) {
										document.getElementById("twitter").innerText = res.value;
										botui.message.add({
											human: true,
											content: res.value
										});
								}).then(function() {
										botui.message.add({
											delay: 1000,
											content: 'Question 4'
										});
								}).then(function() {
										botui.message.add({
											delay: 2000,
											content: 'Are you in any way utilizing our service'
										});	
										botui.message.add({
											delay: 3000,
											content: 'in affiliation with a Company Brand or as an Individual'
										});	
								}).then(function() {
										return botui.action.button({
											delay: 4000,
											action: [{
												text: 'Company Brand',
												value: 'Brand'
											}, {
												text: 'Individual',
												value: 'Individual'
											}]
										})
								}).then(function(res) {
										if (res.value == 'Brand') {
											document.getElementById('saveBrand').innerText = res.value;
											botui.message.add({
												human: true,
												content: res.value
											});
											botui.message.add({
												content: 'Name of Brand'
											}).then(function() {
												return botui.action.text({
													delay: 1000,
													addMessage: false,
													action: {
														sub_type: 'text',
														placeholder: 'Brand Name'
													}
												})
											}).then(function(res) {
												document.getElementById('brandname').innerText = res.value;
												botui.message.add({
													delay: 2000,
													human: true,
													content: res.value
												});
												botui.message.add({
													delay: 3000,
													content: 'Website of Brand'
												});
											}).then(function() {
												return botui.action.text({
													delay: 4000,
													addMessage: false,
													action: {
														sub_type: 'text',
														placeholder: 'https://'
													}
												})
											}).then(function(res) {
												document.getElementById('brandwebsite').innerText = res.value;
												botui.message.add({
													human: true,
													content: res.value
												});
												botui.message.add({
													content: 'Your position at this brand'
												});
											}).then(function() {
												return botui.action.text({
													delay: 1000,
													addMessage: false,
													action: {
														sub_type: 'text',
														placeholder: 'Brand Position'
													}
												})
											}).then(function(res) {
												document.getElementById('brandposition').innerText = res.value;
												botui.message.add({
													human: true,
													content: res.value
												});
											}).then(function () {
												botui.message.add({
													delay: 1000,
													content: '!(check) All Done, Thank You For Your Testimonial!'
												});
											}).then(function () {
												submitTestimonial();
											})
										}
										
										else {
											document.getElementById('saveBrand').innerText = res.value;
											botui.message.add({
												delay: 1000,
												content: '!(check) All Done, Thank You For Your Testimonial!'
											}).then(function () {
												submitTestimonial();
											})
										}
								})
							}
								
				})
	}
	
	function displayNotifier(company) {
		if (company) {
			document.getElementById('testimonly-company').innerText = company;
		}
		var notifier = document.getElementById('testimonly-notifier');
		notifier.style.display = 'block';
		var number = document.getElementById('testimonly-notice');
		number.style.display = 'block';
		notifier.addEventListener('click', function() {
			document.getElementById('testimonly-live-chat').style.display = 'block';
			document.getElementById('testimonly-notice').style.display ='none';
		}, false);
	};
		
	
	
	async function submitTestimonial() {
		var experience = document.getElementById('experience').innerText;
		var testimonial = document.getElementById('testimonial').innerText;
		var tag = document.getElementById('tag').innerText;
		var newTotalTestimonialNumber = parseInt(document.getElementById('testimonialNo').innerText) + 1;
		var twitterLink = "https://www.twitter.com/" + document.getElementById('twitter').innerText;
		var brandname = document.getElementById('brandname').innerText;
		var brandwebsite = document.getElementById('brandwebsite').innerText;
		var brandposition = document.getElementById('brandposition').innerText;
		var email = document.getElementById('email').innerText;
		var username = document.getElementById('username').innerText;
		var date = new Date();
		var saveBrand = document.getElementById('saveBrand').innerText;
		if (saveBrand === 'Brand') {
			var clientTestimonial = {
				npScore: experience,
				pictureLink: "",
				text: testimonial,
				tag : tag,
				date : date,
				twitterLink : twitterLink,
				brandname : brandname,
				brandwebsite : brandwebsite,
				brandposition : brandposition
			};
			
			var userTestimonial = {
				npScore: experience,
				date: date,
				email: email,
				pictureLink: "",
				text : testimonial,
				tag : tag,
				username : username,
				twitterLink : twitterLink,
				brandname : brandname,
				brandwebsite : brandwebsite,
				brandposition : brandposition
			};
		}
		
		else {
			var clientTestimonial = {
				npScore: experience,
				pictureLink: "",
				text: testimonial,
				tag : tag,
				date : date,
				twitterLink : twitterLink,
			};
			
			var userTestimonial = {
				npScore: experience,
				date: date,
				email: email,
				pictureLink: "",
				text : testimonial,
				tag : tag,
				username : username,
				twitterLink : twitterLink,
			};
		}
		
		userTestimonial = JSON.stringify(userTestimonial);
		clientTestimonial = JSON.stringify(clientTestimonial);
		var url = new URL ('https://us-central1-testimonly-7f52e.cloudfunctions.net/storeTestimonial');
		var params = {userTestimonial:userTestimonial, email:email, apiKey:apiKey, clientTestimonial:clientTestimonial, newTotalTestimonialNumber : newTotalTestimonialNumber}; // or:

		url.search = new URLSearchParams(params);

		let response = await fetch(url);
				
		let data = await response.json();			
	

	};
	
	async function submitFeedback() {
		var experience = document.getElementById('experience').innerText;
		var email = document.getElementById('email').innerText;
		var apiKey = document.getElementById('apiKey').innerText;
		var improve = document.getElementById('improve').innerText;
		var newTotalFeedbackNumber = parseInt(document.getElementById('total').innerText);
		var feedback = { 
					1 : { 
							isAdmin : true,
							message : "On a scale of 1-10, how likely are you to recommend us?",
							sent : new Date()
						},
					2 : {
							isAdmin : false,
							message : experience,
							sent : new Date()
						},
					3 : { 
							isAdmin : true,
							message : "In what areas should we improve our services to serve you better",
							sent : new Date()
						},
					4 : { 
							isAdmin : false,
							message : improve,
							sent : new Date()
						}
				};
		feedback = JSON.stringify(feedback)
		var url = new URL('https://us-central1-testimonly-7f52e.cloudfunctions.net/storeFeedback');
		var params = { email:email, apiKey:apiKey, feedback:feedback, newTotalFeedbackNumber:newTotalFeedbackNumber}; // or:
		
		url.search = new URLSearchParams(params);

		let response = await fetch(url);
				
		let data = await response.json();			
	

	};
	
	async function saveClientTime(time) {
		var email = document.getElementById('email').innerText;
		var apiKey = document.getElementById('apiKey').innerText;
		var url = new URL('https://us-central1-testimonly-7f52e.cloudfunctions.net/updateClientTime');
		var params = {email:email, apiKey:apiKey, time:time};
		
		url.search = new URLSearchParams(params);
		
		let response = await fetch(url);
		
		let data = await response.json();
		
	}