var cancel = document.getElementById('testimonly-cancel');
		
		cancel.addEventListener('click', function() {
			document.getElementById('testimonly-live-chat').style.display = 'none';
			document.getElementById('testimonly-notifier').style.display ='none';
		}, false);
		

function displayNotifier(company) {
		if (company) {
			document.getElementById('company').innerText = company;
		}
		var notifier = document.getElementById('testimonly-notifier');
		notifier.style.display = 'block';
		var number = document.getElementById('testimonly-notice');
		number.style.display = 'block';
		notifier.addEventListener('click', function() {
			document.getElementById('testimonly-live-chat').style.display = 'block';
			document.getElementById('testimonly-notice').style.display ='none';
		}, false);
	};
	


function getTestimonial() { 
		var botui = new BotUI('hello-world');
		botui.message.add({ // show a message
			cssClass: 'bot-class',
			content: 'How has your experience been so far viewing this page?'
		}).then(function () { // wait till its shown
			return botui.action.button({
				addMessage: false,
				delay: 1000,
				action: [{
					text: 'Very Bad!',
					value: 'Very Bad!'
				}, {
					text: 'Fair!',
					value: 'Fair!'
				}, {
					text: 'Very Pleasant!',
					value: 'Very Pleasant!'
				}]
			})
		}).then(function (res) {
			botui.message.add({ // show a message
				human: true,
				cssClass: 'human-class',
				content: res.value
			})
			document.getElementById("experience").innerText = res.value;
			if (res.value == "Very Pleasant!"){
				botui.message.add({
					cssClass: 'bot-class',
					delay: 1000,
					content: 'We are very delighted to know that you have found our service offering very pleasant so far'
				}).then(function () {
					botui.message.add({
						cssClass: 'bot-class',
						delay: 1000,
						content: 'And we would absolutely love to have your testimonial'
					});
				}).then(function () {
					botui.message.add({
						cssClass: 'bot-class',
						delay: 2000,
						content: 'On our landing page in three quick questions'
					})
				}).then(function () {
					botui.message.add({
						cssClass: 'bot-class',
						delay: 3000,
						content: 'Question 1 '
					}).then(function () {
						botui.message.add({
							cssClass: 'bot-class',
							delay: 1000,
							content: 'What exactly do you like the most about our service'
						})
					})
				}).then(function () {
					return botui.action.text({
						delay: 4000,
						addMessage: false,
						action: {
							sub_type: 'text',
							placeholder: 'Enter your text here'
						}
					})
				}).then(function (res) {
					document.getElementById("testimonial").innerText = res.value;
					botui.message.add({
						human: true,
						content: res.value
					});
				}).then(function () {
					botui.message.add({
						cssClass: 'question-class',
						delay: 1000,
						content: 'Question 2 '
					}).then(function () {
						botui.message.add({
							cssClass: 'bot-class',
							content: 'Select a tag that summarizes your text above'
						})
					})
				}).then(function () {
						return botui.action.button({
								addMessage: false,
								delay: 2000,
								action: [{
									text: 'General Service',
									value: 'General Service!'
								}, {
									text: 'Design!',
									value: 'Design!'
								},{
									text: 'Customer Support',
									value: 'Customer Support'
								},{
									text: 'Other!',
									value: 'Other!'
								}]
							})
				}).then(function (res) { // get the result
							if(res.value == 'Other!') {
								botui.action.text({
									addMessage: false,
									delay: 1000,
									action: {
										sub_type: 'text',
										placeholder: 'Enter tag here'
									}
								}).then(function (res) {
									document.getElementById("tag").innerText = res.value;
									botui.message.add({
										human: true,
										content: res.value
									});
								}).then(function () {
									botui.message.add({
										cssClass: 'bot-class',
										delay: 2000,
										content: 'Question 3'
									})
								}).then(function () {
										botui.message.add({
											cssClass: 'bot-class',
											delay: 3000,
											content: 'We would like to have your twitter account link'
										})
								}).then(function() {
										botui.message.add({
											cssClass: 'bot-class',
											delay: 4000,
											content: 'As we will like to showcase this as a testimonial on our front page'
										})
								}).then(function() {
										botui.message.add({
											cssClass: 'bot-class',
											delay: 5000,
											content: 'And we will need your twitter username as proof'
										})
											
								}).then(function() {
										return botui.action.text({
											delay: 1000,
											addMessage: false,
											action: {
												sub_type: 'text',
												placeholder: 'https://www.twitter.com/'
											}
										})
								}).then(function(res) {
										document.getElementById("twitter").innerText = res.value;
										botui.message.add({
											delay: 1000,
											human: true,
											content: res.value
										});
								}).then(function() {
										botui.message.add({
											delay: 2000,
											content: 'Question 4'
										});
								}).then(function() {
										botui.message.add({
											delay: 3000,
											content: 'Are you in any way utilizing our service'
										});	
										botui.message.add({
											delay: 4000,
											content: 'in affiliation with a Company Brand or as an Individual'
										});	
								}).then(function() {
										return botui.action.button({
											addMessage: false,
											delay: 5000,
											action: [{
												text: 'Company Brand',
												value: 'Brand'
											}, {
												text: 'Individual',
												value: 'Individual'
											}]
										})
								}).then(function(res) {
										if (res.value == 'Brand') {
											botui.message.add({
												human: true,
												content: res.value
											});
											botui.message.add({
												content: 'Name Of Brand'
											}).then(function() {
												return botui.action.text({
													delay: 1000,
													addMessage: false,
													action: {
														sub_type: 'text',
														placeholder: 'Brand Name'
													}
												})
											}).then(function(res) {
												document.getElementById('brandname').innerText = res.value;
												botui.message.add({
													delay: 2000,
													human: true,
													content: res.value
												});
												botui.message.add({
													delay: 3000,
													content: 'Website Of This Brand'
												});
											}).then(function() {
												return botui.action.text({
													delay: 4000,
													addMessage: false,
													action: {
														sub_type: 'text',
														placeholder: 'https://'
													}
												})
											}).then(function(res) {
												document.getElementById('brandwebsite').innerText = res.value;
												botui.message.add({
													human: true,
													content: res.value
												});
												botui.message.add({
													delay: 1000,
													content: 'Your Position At This Brand'
												});
											}).then(function() {
												return botui.action.text({
													delay: 2000,
													addMessage: false,
													action: {
														sub_type: 'text',
														placeholder: 'Brand Position'
													}
												})
											}).then(function(res) {
												document.getElementById('brandposition').innerText = res.value;
												botui.message.add({
													human: true,
													content: res.value
												});
											}).then(function () {
												botui.message.add({
													delay: 1000,
													content: '!(check) All Done, Thank You For Your Testimonial!'
												});
											}).then(function () {
												submitTestimonial();
											})
										}
										
										else {
											botui.message.add({
												delay: 1000,
												content: '!(check) All Done, Thank You For Your Testimonial!'
											}).then(function () {
												submitTestimonial();
											})
										}
								})
							}
							else {
								document.getElementById("tag").innerText = res.value;
								botui.message.add({
									human: true,
									content: res.value
								}).then(function () {
									botui.message.add({
										cssClass: 'bot-class',
										delay: 1000,
										content: 'Question 3'
									})
								}).then(function (res) {
										botui.message.add({
											cssClass: 'bot-class',
											delay: 2000,
											content: 'We would like to have your twitter account link'
										})
								}).then(function() {
											botui.message.add({
												cssClass: 'bot-class',
												delay: 3000,
												content: 'As we will like to showcase this as a testimonial on our front page'
											})
								}).then(function() {
											botui.message.add({
												cssClass: 'bot-class',
												delay: 4000,
												content: 'And we will need your twitter link as proof'
											})
											
								}).then(function() {
										return botui.action.text({
											delay: 5000,
											addMessage: false,
											action: {
												sub_type: 'text',
												placeholder: 'Enter your text here'
											}
										})
								}).then(function(res) {
										document.getElementById("twitter").innerText = res.value;
										botui.message.add({
											human: true,
											content: res.value
										});
								}).then(function() {
										botui.message.add({
											delay: 1000,
											content: 'Question 4'
										});
								}).then(function() {
										botui.message.add({
											delay: 2000,
											content: 'Are you in any way utilizing our service'
										});	
										botui.message.add({
											delay: 3000,
											content: 'in affiliation with a Company Brand or as an Individual'
										});	
								}).then(function() {
										return botui.action.button({
											delay: 4000,
											action: [{
												text: 'Company Brand',
												value: 'Brand'
											}, {
												text: 'Individual',
												value: 'Individual'
											}]
										})
								}).then(function(res) {
										if (res.value == 'Brand') {
											botui.message.add({
												human: true,
												content: res.value
											});
											botui.message.add({
												content: 'Name of Brand'
											}).then(function() {
												return botui.action.text({
													delay: 1000,
													addMessage: false,
													action: {
														sub_type: 'text',
														placeholder: 'Brand Name'
													}
												})
											}).then(function(res) {
												document.getElementById('brandname').innerText = res.value;
												botui.message.add({
													delay: 2000,
													human: true,
													content: res.value
												});
												botui.message.add({
													delay: 3000,
													content: 'Website of Brand'
												});
											}).then(function() {
												return botui.action.text({
													delay: 4000,
													addMessage: false,
													action: {
														sub_type: 'text',
														placeholder: 'https://'
													}
												})
											}).then(function(res) {
												document.getElementById('brandwebsite').innerText = res.value;
												botui.message.add({
													human: true,
													content: res.value
												});
												botui.message.add({
													content: 'Your position at this brand'
												});
											}).then(function() {
												return botui.action.text({
													delay: 1000,
													addMessage: false,
													action: {
														sub_type: 'text',
														placeholder: 'Brand Position'
													}
												})
											}).then(function(res) {
												document.getElementById('brandposition').innerText = res.value;
												botui.message.add({
													human: true,
													content: res.value
												});
											}).then(function () {
												botui.message.add({
													delay: 1000,
													content: '!(check) All Done, Thank You For Your Testimonial!'
												});
											}).then(function () {
												submitTestimonial();
											})
										}
										
										else {
											botui.message.add({
												delay: 1000,
												content: '!(check) All Done, Thank You For Your Testimonial!'
											}).then(function () {
												submitTestimonial();
											})
										}
								})
							}
								
				})
			}
			
			else {
				botui.message.add({
					content: 'What part of our service do you think we should improve on?'
				}).then(function () {
					return botui.action.button({
						addMessage: false,
						delay: 1000,
						action: [{
							text: 'Our Support',
							value: 'Support!'
						}, {
							text: 'Our Design!',
							value: 'Design!'
						},{
							text: 'Our Customer Support',
							value: 'Customer Support'
						},{
							text: 'Other!',
							value: 'Other!'
						}]
					})
				}).then(function (res) { // get the result
					if(res.value == 'Other!') {
						botui.action.text({
							addMessage: false,
							delay: 1000,
							action: {
								sub_type: 'text',
								placeholder: 'Enter your feedback here'
							}
						}).then(function (res) {
							document.getElementById("improve").innerText = res.value;
							botui.message.add({
								human: true,
								content: res.value
							});
						}).then(function (res) {
							botui.message.add({
								delay: 1000,
								content: '!(check) All Done, Thank You For Your Feedback!'
							});
						}).then(function () {
							submitFeedback();
						})
					}
			
					else {
						document.getElementById("improve").innerText = res.value;
						botui.message.add({
							human: true,
							content: 'Your ' + res.value
						}).then(function () {
							botui.message.add({
								delay: 1000,
								content: '!(check) All Done, Thank You For Your Feedback!'
							});
						}).then(function () {
							submitFeedback();
						})
					}	
				})
			}
		})		
	};
	
	function submitTestimonial() {
		var experience = document.getElementById('experience').innerText;
		var testimonial = document.getElementById('testimonial').innerText;
		var tag = document.getElementById('tag').innerText;
		var newTotalTestimonialNumber = parseInt(document.getElementById('testimonialNo').innerText) + 1;
		var twitterLink = "https://www.twitter.com/" + document.getElementById('twitterLink').innerText;
		var brandname = document.getElementById('brandname').innerText;
		var brandwebsite = document.getElementById('brandwebsite').innerText;
		var brandposition = document.getElementById('brandposition').innerText;
		var email = document.getElementById('email').innerText;
		var username = document.getElementById('username').innerText;	
	};
	
	function submitFeedback() {
		var experience = document.getElementById('experience').innerText;
		var email = document.getElementById('email').innerText;
		var apiKey = document.getElementById('apiKey').innerText;
		var improve = document.getElementById('improve').innerText;
		var newTotalFeedbackNumber = parseInt(document.getElementById('total').innerText);
	};
	
